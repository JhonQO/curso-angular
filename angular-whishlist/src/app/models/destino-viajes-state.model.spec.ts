import { DestinosViajesState, initializeDestinosViajesState, InitMyDataAction, reduceDestinosViajes, NuevoDestinoAction } from "./destino-viajes-state.model"
import { DestinoViaje } from './destino-viaje.model';

describe('redecurDestinosViajes', ()=> {
    it('should reduce init data', ()=> {
        //setup
        const prevState:DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino2']);
        
        //action
        const newState: DestinosViajesState = reduceDestinosViajes(prevState, action);

        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');

        //tear down
    });

    it('should reduce new item added', () =>{
        const prevState: DestinosViajesState= initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
        const newState: DestinosViajesState = reduceDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1)
        expect(newState.items[0].nombre).toEqual('barcelona');
    })

})