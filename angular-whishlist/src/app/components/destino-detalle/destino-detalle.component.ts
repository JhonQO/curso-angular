import { Component, OnInit, Injectable, InjectionToken, Inject, ViewEncapsulation } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { DestinoApiClient } from '../../models/destino-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';

/*
@Injectable({ providedIn: 'root'})
class DestinoApiClientViejo {
  getById(id: String): DestinoViaje {
    console.log('Llamando por la Clase Vieja!')
    return new DestinoViaje(null, null);
  }
}

interface AppConfig {
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {apiEndpoint: 'mi api.com'}

const APP_CONFIG = new InjectionToken<AppConfig>('app.config')

class DestinoApliClientDecorated extends DestinoApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }

  getById(id: String) : DestinoViaje {
    console.log('Llamando por la clase decorada!')
    console.log('config'+this.config.apiEndpoint);
    return super.getById(id);
  }
}
*/

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    DestinoApiClient
    // DestinoApiClient, {provide: DestinoApiClientViejo, useExisting: DestinoApiClient}]
   /* {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    {provide: DestinoApiClient, useFactory: DestinoApliClientDecorated},
    {provide: DestinoApiClientViejo, useExisting: DestinoApiClientViejo}
    */
  ]
})
export class DestinoDetalleComponent implements OnInit {

  public destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version:8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  }

  //constructor(private route: ActivatedRoute, private destinoApiClient: DestinoApiClientViejo) { }
  constructor(private route: ActivatedRoute, private destinoApiClient: DestinoApiClient) { }

  ngOnInit(): void {

    let id = this.route.snapshot.paramMap.get("destinoId");
    this.destino =  this.destinoApiClient.getById(id);
    if(this.destino == null){
      this.destino = new DestinoViaje(null, null);
      this.destino.destinoId=id;
    }
  }

}
