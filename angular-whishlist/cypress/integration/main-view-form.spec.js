describe('Formulario', () => {
    it('Tiene dos input, una autocomplete y un botton button', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-whishlist');
        cy.get("input[name=nombre]").type('Barcelona{enter}').should('have.value', 'Barcelona');
        cy.get("form ul li").first().should('contain', 'Barcelona');
        cy.get("input[name=imagenUrl]").type('abc@abc.com{enter}').should('have.value', 'abc@abc.com');

        cy.get("form button").click();

    })

})