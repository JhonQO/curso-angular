import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective implements OnInit, OnDestroy {
  static nextId=0;

  log = (msg:string) => console.log(`Evento #${EspiameDirective.nextId++} ${msg}`)
  constructor() {
    this.log('####################*************    Construnctor')
   }

  ngOnDestroy(): void {
    this.log('####################*************   OnDestroy  ')
  }
  ngOnInit(): void {
    this.log('####################*************    OnInit')
  }


}
