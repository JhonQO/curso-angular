import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction, EliminarDestinoAction, ResetVotosAction } from './destino-viajes-state.model';
import { HttpClient, HttpRequest, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DestinoApiClient{

    constructor(private store: Store<AppState>, @Inject(forwardRef( ()=> APP_CONFIG)) private config: AppConfig, private http: HttpClient){ 
     }

     add(d: DestinoViaje){
         // this.store.dispatch(new NuevoDestinoAction(d));
         const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
         const req = new HttpRequest('POST', this.config.apiEndpoint+ '/my', {nuevo: d.nombre}, {headers:headers});
         this.http.request(req).subscribe((data: HttpResponse<{}>) => {
             if(data.status === 200){
                 this.store.dispatch(new NuevoDestinoAction(d));
                 const myDb = db;
                 myDb.destinos.add(d);
                 console.log('Todos los destinos de la bd!')
                 myDb.destinos.toArray().then(destinos=> console.log(destinos));
             }
         })
     }
     
     elegir(d: DestinoViaje){
         this.store.dispatch(new ElegidoFavoritoAction(d));
     }
     
     eliminar(d: DestinoViaje){
         
         this.store.dispatch(new EliminarDestinoAction(d));
     }

     reset(d:DestinoViaje) {
         this.store.dispatch(new ResetVotosAction(d));
     }

    getById(id:String): DestinoViaje{
        console.log("Nuevo Element")
        return null;
    }
     

}