import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guia-uso',
  templateUrl: './guia-uso.component.html',
  styleUrls: ['./guia-uso.component.css']
})
export class GuiaUsoComponent implements OnInit {

  instrucciones: String[];
  constructor() { 
    this.instrucciones = [];
    this.instrucciones.push("Ingrese los datos del formulario.");
    this.instrucciones.push("Click en el boton: Guardar, para añadir WhishLists.");
    this.instrucciones.push("Click en limpiar para borrar los campos del fomulario");
    this.instrucciones.push("Repetir el paso 1");
  }

  ngOnInit(): void {
  }

}
