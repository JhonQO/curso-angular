describe('Intrucciones', () => {
    it('Tiene un boton para desplegar y luego mostrar las intrucciones', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-whishlist');
        cy.get("div button").click();
        cy.get("ol li").first().should('contain', 'Ingrese los datos del formulario.');
        cy.get('ol>li').eq(0).should('contain', 'Ingrese los datos del formulario.')
        cy.get('ol>li').eq(1).should('contain', 'Click en el boton: Guardar, para añadir WhishLists')
        cy.get('ol>li').eq(3).should('contain', 'Repetir el paso 1')


    })

})