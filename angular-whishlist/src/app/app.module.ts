import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER, Inject } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ActionReducerMap, StoreModule as NgRxStoreModule, Store, State } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http'
import Dexie from 'dexie';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxMapboxGLModule} from 'ngx-mapbox-gl';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { GuiaUsoComponent } from './components/guia-uso/guia-uso.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component'
import { DestinosViajesState, reduceDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects, InitMyDataAction } from './models/destino-viajes-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// app config
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {apiEndpoint: 'http://localhost:3000'}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config')
// fin app config

// Init router
const reouteChildrenVuelos : Routes = [
  {path:'', redirectTo: 'main', pathMatch:'full'},
  {path: 'main', component: VuelosMainComponent},
  {path: 'mas-info', component: VuelosMasInfoComponent},
  {path: ':id', component: VuelosDetalleComponent}
]
const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch:'full'},
  {path: 'home', component:ListaDestinosComponent},
  {path: 'destino', component:DestinoDetalleComponent},
  {path: 'destino/:destinoId', component:DestinoDetalleComponent},
  {path: 'login', component: LoginComponent},
  //Accedo si solo estoy logueado:
  {path: 'protected', component: ProtectedComponent, canActivate: [UsuarioLogueadoGuard]},
  {path: 'vuelos', component: VuelosComponent, canActivate: [UsuarioLogueadoGuard], children: reouteChildrenVuelos}

];
// fin router

// Redux: init
// estado global
export interface AppState{
  destinos: DestinosViajesState;
}
// join with the reducer, reducars gglobales de la apllicacion.
const reducers: ActionReducerMap<AppState> = {
  destinos: reduceDestinosViajes
}

const reducersInitialState = {
  destinos: initializeDestinosViajesState()
}
// Redux: fin init

//app Init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable({providedIn: 'root'})
class AppLoadService {
  constructor (private store: Store<AppState>, private http: HttpClient) {}

  async initializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint+ '/my', {headers: headers});
    const response:any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
//fin app Init

// Dexie DB
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value:string){}
}

@Inject({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation,number>
  constructor () {
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl'
    });
    this.version(2).stores({
      destinos: '++id, nombre, imageUrl',
      translations: '++id, lang, key, value'
    })
  }
}

export const db = new  MyDatabase();
// fin dexie DB


// i18n init
@Inject({
  providedIn: 'root'
})
class TranslationLoader implements TranslateLoader {
  constructor(public http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang').equals(lang).toArray().then(results => {
                        if(results.length===0){
                          return this.http.get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang='+lang)
                                          .toPromise()
                                          .then(apiResults=> {
                                            db.translations.bulkAdd(apiResults);
                                            return apiResults;
                                          });
                        }
                        return results;
                      }).then((traducciones) => {
                        console.log("Traducciones cargadas: ")
                        console.log(traducciones);
                        return traducciones;
                      }).then((traducciones) => {
                        return traducciones.map((t=> ({ [t.key] : t.value }) ))
                      });
    return from(promise).pipe(flatMap( (elems) => from(elems)));

  }

}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

// Fin i18n
@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    GuiaUsoComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, 
      {initialState: reducersInitialState,  
        runtimeChecks: {
          strictStateImmutability: false,
          strictActionImmutability: false,
        }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({
      //maxAge: 25, // Retains last 25 states
     // logOnly: environment.production, // Restrict extension to log-only mode
    }),
    ReservasModule,
    //Importat las traducciones para toda la raiz.
    TranslateModule.forRoot({
      loader: {provide:TranslateLoader, useFactory:(HttpLoaderFactory), deps: [HttpClient]},
      //useDefaultLang: false
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard,
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    // se ejecuta al iniciar Angular, 
    AppLoadService, {provide: APP_INITIALIZER, useFactory: init_app, deps:[AppLoadService], multi:true},
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
